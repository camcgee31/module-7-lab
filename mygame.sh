#!/bin/bash

# card maching game
# Runn this script as sh ./mygame.sh

#This function is the graffics It uses the cards array to see what you have found and what you solected 
table()
{
echo "|-------------||-------------||-------------|"
echo "|             ||             ||             |"
echo "|      "${cards[0]}"     1||     "${cards[1]}"      2||     "${cards[2]}"      3|"
echo "|             ||             ||             |"
echo "|_____________||_____________||_____________|"
echo "|-------------||-------------||-------------|"
echo "|             ||             ||             |"
echo "|      "${cards[3]}"     4||      "${cards[4]}"     5||     "${cards[5]}"      6|"
echo "|             ||             ||             |"
echo "|_____________||_____________||_____________|"
echo "|-------------||-------------||-------------|"
echo "|             ||             ||             |"
echo "|      "${cards[6]}"     7||      "${cards[7]}"     8||     "${cards[8]}"      9|"
echo "|             ||             ||             |"
echo "|_____________||_____________||_____________|"
echo "|-------------||-------------||-------------|"
echo "|             ||             ||             |"
echo "|      "${cards[9]}"    10||      "${cards[10]}"    11||     "${cards[11]}"     12|"
echo "|             ||             ||             |"
echo "|_____________||_____________||_____________|"
} 

clear

# Waits until user is ready
echo "Hit enter to play!"
read 

#Set start time. This is used to track how long it takes the user to win
start=`date +%s`

#Set win to 0 this is used to deturmen if you won or not
win=0

#Set up cards array. 
newcards=( "A" "A" "B" "B" "C" "C" "D" "D" "E" "E" "F" "F" )

# Shuffle the array so each game is new 
newcards=( $(shuf -e "${newcards[@]}") )

# Set cards to blanks. This sets the display to all blanks 
cards=( " " " " " " " " " " " " " " " " " " " " " " " " )

userinput=" "

# This array is used to track which number the user has already guessed
alreadywonenum=()
#while user has not input exit then
while [ "userinput" != "exit" ]
do
	
	#display graphics
	table 
	
	# get first number
	echo ""
	echo " Guess a card ! type its number"
	read userinput
############################################################################
	((userinput--))
# If user inputed anything but a number and not inbetween 1 and 12
# Then reset user inputs and go back to the beginning 
	if [ "$userinput"  -lt 0 -o "$userinput" -gt 12 ]
	then
		clear
		echo " Pick a number shown in the cards"
		cards[$userinput]=" "
		continue 
	fi
	#Sice if a user inputs 1 then that is 0 in array so you have to -- it one left to fit the array 
############################################################################
#checks if user has picked a number they have already won
# what this does is it sets yes to 0. If you find that you have guessed a number you have already won then yes will be set to one and you will be set back to the beginning
	yes=0
	echo "${alreadywonenum[@]}"
	for i in "${alreadywonenum[@]}"
	do
		
		if [ "$i" = "$userinput" ]
		then
			clear
			echo " You cant pick nubers that you have won already"
			yes=1
		fi
	done
	if [ $yes -eq 1 ]
	then
		continue
	fi
############################################################################
	cards[$userinput]="${newcards[$userinput]}"
	
	clear
	table
	
	#Gets another user input second number/card 
	echo ""
	echo " Guess another card ! type its number"
	read userinput2
	
############################################################################
# # If user inputed anything but a number and not inbetween 1 and 12
# Then reset user inputs and go back to the beginning 
# userinput2-- is used to mach user input to the array since user 1 should be 0 
	((userinput2--))
	if [ "$userinput2"  -lt 0 -o "$userinput2" -gt 12 ]
	then
		clear
		echo " Pick a number shown in the cards"
		echo " Try again "
		cards[$userinput]=" "
		cards[$userinput2]=" "
		continue 
	elif [ "$userinput" = "$userinput2" ]
	then 
		clear
		echo "cant pick the same number"
	fi
############################################################################
#checks if user has picked a number they have already won
# what this does is it sets yes to 0. If you find that you have guessed a number you have already won then yes will be set to one and you will be set back to the beginning
	yes=0
	for i in "${alreadywonenum[@]}"
	do
		if [ "$userinput2" = $i ]
		then
			echo " You cant pick nuubers that you have won already"
			yes=1
		fi
	done
	if [ $yes -eq 1 ]
	then
		continue
	fi
############################################################################
	# Show users guess in the graphics 
	cards[$userinput2]="${newcards[$userinput2]}"
	
# If your first input guess = your second input mach then you got a mach and ++ alreadywoenumbers and ++ win
#If not then wait for user to hit enter 	
	if [ ${cards[$userinput]} = ${cards[$userinput2]} ]
	then
		((win++))
		alreadywonenum+=($userinput)
		alreadywonenum+=($userinput2)
	else
		clear
		table
		echo "hit enter to try again"
		read
		cards[$userinput]=" "
		cards[$userinput2]=" "
		
	fi
#############################################################################
# If you get 6 guesses right then you with and display the date
	if [ "$win" = "6" ]
	then
		echo "!!!!!!!!!!You win!!!!!!!!!!"
		end=`date +%s`
		runtime=$((end-start))
		echo "It took you $runtime seconds to finish!"
		exit 0
	fi
	clear
#############################################################################
done
